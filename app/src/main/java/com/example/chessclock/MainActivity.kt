package com.example.chessclock

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.style.BackgroundColorSpan
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.chessclock.ui.main.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.main_activity)
//        if (savedInstanceState == null) {
//            supportFragmentManager.beginTransaction()
//                    .replace(R.id.container, MainFragment.newInstance())
//                    .commitNow()
//        }

        setContent {
            this.SetTitle(name = "CHESS")
        }

    }

    @Composable
    fun SetTitle(name: String) {
        Column(modifier = Modifier.padding(10.dp)) {
            Text(text = "Game of $name")
            Text(text = "Create a new timer")
            Text(text = "Start match")

            Spacer(modifier = Modifier.height(12.dp))
            
            Image(
                painter = painterResource(id = R.drawable.ic_background),
                modifier = Modifier
                    .height(180.dp)
                    .fillMaxWidth(),
                contentDescription = null
            )
        }
    }
}